SUMMARY = "My app"
SECTION = "examples"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

EXTRA_OECMAKE = " \
    -DCMAKE_BUILD_TYPE=Release \
    -DBAZ=2 \
"

SRC_URI = "git://gitlab.com/cjmeyer/my-app.git;protocol=http"
SRCREV = "0cab6b2cd61444e79dbeab306b9c4fae49b15b65"

S = "${WORKDIR}/git"

inherit cmake
