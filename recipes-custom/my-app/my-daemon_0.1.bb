SUMMARY = "Simple helloworld application"
SECTION = "examples"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
SRC_URI = " \
    file://my-daemon.sh \
"

S = "${WORKDIR}"

DEPENDS_append = " update-rc.d-native"

do_install() {
   install -d ${D}${sysconfdir}/init.d

   install -m 0755 my-daemon.sh ${D}${sysconfdir}/init.d
   update-rc.d -r ${D} my-daemon.sh start 99 5 .
}
