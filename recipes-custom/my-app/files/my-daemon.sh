#! /bin/sh

# Carry out specific functions when asked to by the system
case "$1" in
start)
    echo "Starting my-daemon..."
    ;;
stop)
    echo "Stopping my-daemon..."
    ;;
*)
    echo "Usage: /etc/init.d/my-daemon.sh {start|stop}"
    exit 1
    ;;
esac
exit 0
