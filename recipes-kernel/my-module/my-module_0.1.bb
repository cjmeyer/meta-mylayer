SUMMARY = "Example of how to build an external Linux kernel module"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

inherit module

SRC_URI = " \
  file://Makefile \
  file://my-module.c \
"

S = "${WORKDIR}"

RPROVIDES_${PN} += "kernel-module-my-module"

KERNEL_MODULE_AUTOLOAD += "my-module"

